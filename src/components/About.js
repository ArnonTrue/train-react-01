import React, { useState } from "react";

const About = () => {
  const [name, setName] = useState("");
  const [age, setAge] = useState("");

  const clickSubmit = () =>{
      let userInfo = {
          name,
          age
      }

      console.log(userInfo);
  }

  //console.log(name);
  return (
    <div>
      <h1>About Page</h1>
      <label>name</label>
      <input
        value={name}
        type="text"
        onChange={(e) => {
          setName(e.target.value);
        }}
      ></input>
      <label>age</label>
      <input
        value={age}
        type="text"
        onChange={(e) => {
          setAge(e.target.value);
        }}
      ></input>
      <button type="button" onClick={clickSubmit}>Submit</button>
    </div>
  );
};

export default About;
