import React from "react";

const Home = (props) => {
  console.log(props);

  //let name = props.name;
  //let age = props.age;

  let {name="no name", age="0"} = props;

  return (
    <div>
      <h1>Home Page {name} {age}</h1>
    </div>
  );
};

export default Home;
