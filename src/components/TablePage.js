import React from "react";
import Table from "./Table";
import styled from "styled-components";
//import { useTable } from 'react-table'

const TablePage = () => {
  const Styles = styled.div`
    padding: 1rem;
    table {
      border-spacing: 0;
      border: 1px solid black;
      tr {
        :last-child {
          td {
            border-bottom: 0;
          }
        }
      }
      th,
      td {
        margin: 0;
        padding: 0.5rem;
        border-bottom: 1px solid black;
        border-right: 1px solid black;
        :last-child {
          border-right: 0;
        }
      }
    }
  `;

  let data = [
    { username: "Arm", age: "18" },
    { username: "Arm2", age: "182" },
  ];

  const columns = [
    {
      Header: "Username",
      accessor: "username",
    },
    {
      Header: "Age",
      accessor: "age",
    },
  ];
  return (
    <div>
      <h1>Table Page</h1>
      <hr></hr>
      <Styles>
        <Table columns={columns} data={data}></Table>
      </Styles>
    </div>
  );
};

export default TablePage;
