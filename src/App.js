import React from "react";
import logo from "./logo.svg";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import "./App.css";
import Home from "./components/Home";
import About from "./components/About";
import TablePage from './components/TablePage';

function App() {
  return (
    <>
      <Router>
        <div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/table">Table</Link>
            </li>
          </ul>
        </div>
        <hr></hr>

        <Switch>
          <div className="App">
            <Route exact path="/">
              <Home name="Arm" age="18"></Home>
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/table">
              <TablePage />
            </Route>
          </div>
        </Switch>
      </Router>
    </>
  );
}

export default App;
